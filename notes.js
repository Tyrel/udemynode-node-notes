const fs = require('fs');

var fetchNotes = () => {
  try {
    var notesString = fs.readFileSync('notes-data.json');
    return JSON.parse(notesString);
  } catch (e) {
    console.log("File doesn't exist, making new file.");
    return [];
  }
}

var saveNotes = (notes) => {
  fs.writeFileSync(
    'notes-data.json',
    JSON.stringify(notes)
  );
}

var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body,
  };
  var duplicateNotes = notes.filter((note) => note.title === title);

  if (duplicateNotes.length === 0) {
    notes.push(note);
    saveNotes(notes);
    console.log(`Added note: ${note.title}`);
    console.log("--");
    return note;
  }
}
var getAll = () => {
     return fetchNotes();
}

var getNote = (title) => {
    var notes = fetchNotes().filter((note) => note.title === title);
    return notes[0];
}

var removeNote = (title) => {
    var notes = fetchNotes();
    filteredNotes = notes.filter((note) => note.title !== title);
    saveNotes(filteredNotes);
    return notes.length !== filteredNotes.length;
}

var logNote = (note) => {
  console.log(`Title: ${note.title}`);
  console.log(`Body: ${note.body}`);
}

module.exports = {
  logNote,
  addNote,
  getAll,
  getNote,
  removeNote,
}
